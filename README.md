# Stateless Petstore Fork
This is a fork of https://github.com/pauljamescleary/scala-pet-store where the stateful JWTAuthentication has been replaced with a stateless implementation.
## Getting Started

Be aware that this project targets **Java 11**.

Start up sbt:

```bash
sbt --java-home {your.java.11.location}
```

Once sbt has loaded, you can start up the application

```sbtshell
> ~reStart
```

This uses revolver, which is a great way to develop and test the application.  Doing things this way the application
will be automatically rebuilt when you make code changes

To stop the app in sbt, hit the `Enter` key and then type:

```sbtshell
> reStop
```

## Testing
Building out a test suite using Python.  The reason is that typically we want to run tests against a live environment
when we deploy our code in order to make sure that everything is running properly in the target environment.  It
is reassuring to know that your code works across clients.

In order to run the functional tests, your machine will need to have Python 3 and pip, and virtualenv.

1. To install pip on a Mac, run `sudo easy_install pip`
2. Then install virutalenv `sudo pip install virtualenv`

To test out the app, first start it up following the directions above and doing `reStart`

Then, in a separate terminal, run the test suite:

```
> cd functional_test
> ./run.py live_tests -v
```

